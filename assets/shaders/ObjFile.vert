#version 330 core
layout (location = 0) in vec4 aPos;
layout (location = 1) in vec3 aNormal;
layout (location = 2) in vec2 aTexCoord;

uniform mat4 view;
uniform mat4 model;
uniform mat4 perspective;

out vec3 Normal;
out vec3 FragPosition;
out vec2 TexCoord;

void main() {
    FragPosition = vec3(model * aPos);
    Normal = mat3(transpose(inverse(model))) * aNormal;
    TexCoord = aTexCoord;
    gl_Position = perspective * view * model * aPos;
    
}
