#version 330 core

in vec3 Normal;
in vec3 FragPosition;
in vec2 TexCoord;

out vec4 FragColor;

uniform vec3 ambientLightColor;
uniform vec3 diffuseLightColor;
uniform vec3 specularLightColor;

uniform vec3 lightPosition;

uniform sampler2D ourTexture;

uniform vec3 cameraPosition;

void main() {
    float ambientStrength = 0.1;
    vec3 ambient = ambientStrength * ambientLightColor;

    vec3 normal = normalize(Normal);
    vec3 lightDirection = normalize(lightPosition - FragPosition);

    float diff = max(dot(normal, lightDirection), 0.0);
    vec3 diffuse = diff * diffuseLightColor;

    float specularStrength = 0.5;
    vec3 viewDir = normalize(cameraPosition - FragPosition);
    vec3 reflectDir = reflect(-lightDirection, normal);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), 32);
    vec3 specular = specularStrength * spec * specularLightColor;


    vec3 result = (ambient + diffuse + specular);
    FragColor = vec4(result, 1.0) * texture(ourTexture, TexCoord);
}
