#include <stdio.h>
#include <stdlib.h>

#include "useful.h"

void imprimeOlaMundo() {
    printf("imprime ola mundo \n");
}

void dizerTchau(int value) {
    printf("Tchau!\n");
}

void behaviourParent() {
    printf("sou o pai\n");
}

void behaviourSon() {
    printf("sou o filho\n");
}
