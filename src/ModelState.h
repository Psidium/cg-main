#ifndef MODEL_STATE_H
#define MODEL_STATE_H

//#include <glad/glad.h> // include glad to get all the required OpenGL headers
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
//#include <Shader.h>
//
//#include <string>
//#include <fstream>
//#include <sstream>
//#include <iostream>
//#include <vector>


class ModelState {
public:
    virtual void addY(glm::mat4* model, float speed) = 0;
    virtual void subY(glm::mat4* model, float speed) = 0;
    virtual void addX(glm::mat4* model, float speed) = 0;
    virtual void subX(glm::mat4* model, float speed) = 0;
    virtual void addZ(glm::mat4* model, float speed) = 0;
    virtual void subZ(glm::mat4* model, float speed) = 0;
};

#endif
