#include "Mtl.h"

#include <boost/filesystem/fstream.hpp>
#include <fstream>
#include <sstream>
#include <iostream>

#define cimg_display 0
#include <CImg.h>

MtlLib::MtlLib(boost::filesystem::path path) {
    boost::filesystem::ifstream mtlFile(path);
    std::string line;
    
    Mtl* currentMtl;
    try {
        while(std::getline(mtlFile, line)) {
            std::stringstream lineStream(line);
            std::string identifier;
            lineStream >> identifier;
            if (identifier == "newmtl") {
                std::string mtlName;
                lineStream >> mtlName;
                currentMtl = &this->mtls[mtlName];
            } else if (identifier == "Ka") {
                glm::dvec3 ka;
                lineStream >> ka.r >> ka.g >> ka.b;
                currentMtl->ambientLightColor = ka;
            } else if (identifier == "Kd") {
                glm::dvec3 kd;
                lineStream >> kd.r >> kd.g >> kd.b;
                currentMtl->difuseLightColor = kd;
            } else if (identifier == "Ks") {
                glm::dvec3 ks;
                lineStream >> ks.r >> ks.g >> ks.b;
                currentMtl->specularLightColor = ks;
            } else if (identifier == "d") {
                lineStream >> currentMtl->d;
            } else if (identifier == "map_Kd") {
                std::string textureFileName;
                lineStream >> textureFileName;
                auto mapPath = path.parent_path();
                mapPath /= textureFileName;
                currentMtl->loadTexture(mapPath);
            }
        }
    }
    catch (std::ifstream::failure e)
    {
        std::cout << "ERROR::MTL::FILE_NOT_SUCCESFULLY_READ" << std::endl;
    }
}

Mtl MtlLib::getMtlByName(std::string name) {
    return this->mtls[name];
}

void Mtl::loadTexture(boost::filesystem::path path) {
    const char * loc = path.string().c_str();
    cimg_library::CImg<unsigned char> image(loc);
    int width = image.width();
    int height = image.height();
    //convert to OpenGL image
    image.permute_axes("cxyz");
    unsigned char* imgPtr = image.data(0,0);
    
    GLuint textureId;
    glGenTextures(1, &textureId);
    glBindTexture(GL_TEXTURE_2D, textureId);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, imgPtr);
    glGenerateMipmap(GL_TEXTURE_2D);
    
    
    glBindTexture(GL_TEXTURE_2D, 0);
    
    this->textId = textureId;
}

int Mtl::getTextureIndex() {
    return this->textId;
}
