#include "ModelRotation.h"

ModelRotation::ModelRotation() {
}

void ModelRotation::addX(glm::mat4 * value, float speed) {
    *value = glm::rotate(*value, glm::radians(45.0f) * speed ,glm::vec3(1.0f, 0.0f, 0.0f));
}

void ModelRotation::addY(glm::mat4 * value, float speed) {
    *value = glm::rotate(*value, glm::radians(45.0f) * speed ,glm::vec3(0.0f, 1.0f, 0.0f));
    
}
void ModelRotation::addZ(glm::mat4 * value, float speed) {
    *value = glm::rotate(*value, glm::radians(45.0f) * speed ,glm::vec3(0.0f, 0.0f, 1.0f));
}

void ModelRotation::subX(glm::mat4 * value, float speed) {
    *value = glm::rotate(*value, glm::radians(-45.0f) * speed, glm::vec3(1.0f, 0.0f, 0.0f));
}

void ModelRotation::subY(glm::mat4 * value, float speed) {
    *value = glm::rotate(*value, glm::radians(-45.0f) * speed ,glm::vec3(0.0f, 1.0f, 0.0f));
}

void ModelRotation::subZ(glm::mat4 * value, float speed) {
    *value = glm::rotate(*value, glm::radians(-45.0f) * speed,glm::vec3(0.0f, 0.0f, 1.0f));
}
