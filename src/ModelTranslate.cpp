#include "ModelTranslate.h"

void ModelTranslate::addX(glm::mat4 * value, float speed) {
    *value = glm::translate(*value, glm::vec3(0.4f * speed, 0.0f, 0.0f));
}

void ModelTranslate::addY(glm::mat4 * value, float speed) {
    *value = glm::translate(*value, glm::vec3(0.0f, speed * 0.4f, 0.0f));
}
void ModelTranslate::addZ(glm::mat4 * value, float speed) {
    *value = glm::translate(*value, glm::vec3(0.0f, 0.0f, speed * 0.4f));
}

void ModelTranslate::subX(glm::mat4 * value, float speed) {
    *value = glm::translate(*value, glm::vec3(-0.4f * speed, 0.0f, 0.0f));
}

void ModelTranslate::subY(glm::mat4 * value, float speed) {
    *value = glm::translate(*value, glm::vec3(0.0f, -0.4f * speed, 0.0f));
}

void ModelTranslate::subZ(glm::mat4 * value, float speed) {
    *value = glm::translate(*value, glm::vec3(0.0f, 0.0f, -0.4f * speed));
}
