#ifndef OBJ_H
#define OBJ_H


#include <glad/glad.h> // include glad to get all the required OpenGL headers
#include "Shader.h"
#include <glm/glm.hpp>
#include <vector>
#include "Mtl.h"

struct Face {
    int vertice[3];
    int vt[3];
    int vn[3];
};

struct Vertex {
    glm::vec4 position;
    glm::vec3 normal;
    glm::vec2 texture;
};

struct Drawable {
    int vao;
    int groupSize;
    int textureId;
    glm::vec3 ambientLightColor;
    glm::vec3 difuseLightColor;
    glm::vec3 specularLightColor;
};

struct Group {
    std::vector<unsigned int> indices;
    Mtl mtl;
};

class Obj {
public:
    Obj(const GLchar* objFilePath, const GLchar* vertexShaderLocation, const GLchar* fragmentShaderLocation);
    ~Obj();
    void draw(glm::mat4 perspective, glm::mat4 model, glm::mat4 view);
    void draw(glm::mat4 perspective, glm::mat4 model, glm::mat4 view, glm::vec3 cameraPos);
    void setLightColor(glm::vec3 lightColor);
    void setObjColor(glm::vec3 objColor);
    void setLightPosition(glm::vec3 lightPosition);
    void createBuffers();
    bool isLamp = false;
private:
    std::vector<Vertex> vertexes;
    std::vector<Group> groups;
    Shader* shader;
    glm::vec3 lightColor = glm::vec3(1.0f);
    glm::vec3 objColor;
    glm::vec3 lightPosition;
    std::vector<Drawable> drawables;
    MtlLib* mtllib;
};

#endif
