#include <iostream>
#include <math.h>

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "Shader.h"
#include "Obj.h"

#include "ModelContext.h"
#define SCREEN_WIDTH 800 
#define SCREEN_HEIGHT 600

//OBS THERE IS A BUG ON GLFW THAT IS FIXED ON 3.3.0

void mouse_callback(GLFWwindow* window, double xpos, double ypos);

int g_width = SCREEN_WIDTH;
int g_height = SCREEN_HEIGHT;

ModelContext context = ModelContext();


float controle = 0.0f;
void framebuffer_size_callback(GLFWwindow* window, int width, int height) {
    g_width = width;
    g_height = height;
    glViewport(0, 0, width, height);
}

unsigned int option = 1;
glm::vec3 cameraPos   = glm::vec3(0.0f, 0.0f,  3.0f);
glm::vec3 cameraFront = glm::vec3(0.0f, 0.0f, -1.0f);
glm::vec3 cameraUp    = glm::vec3(0.0f, 1.0f,  0.0f);

float deltaTime = 0.0f;	// Time between current frame and last frame
float lastFrame = 0.0f; // Time of last frame



void processInput(GLFWwindow *window) {
    float currentFrame = glfwGetTime();
    deltaTime = currentFrame - lastFrame;
    lastFrame = currentFrame;
    float cameraSpeed = 5.0f * deltaTime;
    if(glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, true);
    }
    if(glfwGetKey(window, GLFW_KEY_1) == GLFW_PRESS) {
        option = 1;
    }
    if(glfwGetKey(window, GLFW_KEY_2) == GLFW_PRESS) {
        option = 2;
    }
    if(glfwGetKey(window, GLFW_KEY_3) == GLFW_PRESS) {
        option = 3;
    }

    if(glfwGetKey(window, GLFW_KEY_M) == GLFW_PRESS) {
        controle += 10.0f;
    }

    if(glfwGetKey(window, GLFW_KEY_N) == GLFW_PRESS) {
        controle -= 10.0f;
    }
    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) {
        cameraPos += cameraSpeed * cameraFront;
    }
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {
        cameraPos -= cameraSpeed * cameraFront;
    }
    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) {
        cameraPos -= glm::normalize(glm::cross(cameraFront, cameraUp)) * cameraSpeed;
    }
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) {
        cameraPos += glm::normalize(glm::cross(cameraFront, cameraUp)) * cameraSpeed;
    }
    if (glfwGetKey(window, GLFW_KEY_H) == GLFW_PRESS) {
        //translate left
        //x-
        context.subX(cameraSpeed);
    }
    if (glfwGetKey(window, GLFW_KEY_L) == GLFW_PRESS) {
        //translate right
        //x+
        context.addX(cameraSpeed);
    }
    if (glfwGetKey(window, GLFW_KEY_J) == GLFW_PRESS) {
        //translate down
        //y-
        context.subY(cameraSpeed);
    }
    if (glfwGetKey(window, GLFW_KEY_K) == GLFW_PRESS) {
        //translate up
        //y+
        context.addY(cameraSpeed);
    }
    if (glfwGetKey(window, GLFW_KEY_N) == GLFW_PRESS) {
        //closer
        //z+
        context.addZ(cameraSpeed);
    }
    if (glfwGetKey(window, GLFW_KEY_U) == GLFW_PRESS) {
        //farther
        //z-
        context.subZ(cameraSpeed);
    }
    if (glfwGetKey(window, GLFW_KEY_T) == GLFW_PRESS) {
        //select translate mode
        context.setTranslationState();
    }
    if (glfwGetKey(window, GLFW_KEY_R) == GLFW_PRESS) {
        //select rotate mode
        context.setRotationState();
    }
    if (glfwGetKey(window, GLFW_KEY_B) == GLFW_PRESS) {
        //Scale UP
        context.scaleUp(cameraSpeed);
    }
    if (glfwGetKey(window, GLFW_KEY_V) == GLFW_PRESS) {
        //Scale DOWN
        context.scaleDown(cameraSpeed);
    }
    
    if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS) {
        //space to liberate mouse
        static bool control = false;
        if (control) {
            glfwSetCursorPosCallback(window, mouse_callback);
            glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
            control = false;
        } else {
            glfwSetCursorPosCallback(window, NULL);
            glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
            control = true;
        }
    }
}



void mouse_callback(GLFWwindow* window, double xpos, double ypos) {
    static float lastX = 400, lastY = 300;
    static bool firstMouse = true;
    if(firstMouse) // this bool variable is initially set to true
    {
        lastX = xpos;
        lastY = ypos;
        firstMouse = false;
    }
    float xoffset = xpos - lastX;
    float yoffset = lastY - ypos; // reversed since y-coordinates range from bottom to top
    lastX = xpos;
    lastY = ypos;
    
    float sensitivity = 0.05f;
    xoffset *= sensitivity;
    yoffset *= sensitivity;
    
    static float yaw = -90.0f , pitch;
    yaw   += xoffset;
    pitch += yoffset;
    if(pitch > 89.0f){
        pitch =  89.0f;
    }
    if(pitch < -89.0f){
        pitch = -89.0f;
    }
    
    glm::vec3 front;
    front.x = cos(glm::radians(pitch)) * cos(glm::radians(yaw));
    front.y = sin(glm::radians(pitch));
    front.z = cos(glm::radians(pitch)) * sin(glm::radians(yaw));
    cameraFront = glm::normalize(front);
};

int main(int argc, char** argv) {
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    //required to Mac
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

    GLFWwindow* window = glfwCreateWindow(g_width, g_height, "CG17-2", NULL, NULL);
    if (window == NULL) {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);
    glfwGetFramebufferSize(window, &g_width, &g_height);
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
    
    
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
        std::cout << "Failed to initialize GLAD" << std::endl;
        return -1;
    }
    
    glViewport(0, 0, g_width, g_height);
    glEnable(GL_DEPTH_TEST); 
    std::cout << glfwGetVersionString();
    
    glfwSetCursorPosCallback(window, mouse_callback);
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

    
    auto lightPosition = glm::vec3(0.0f, 5.0f, 5.0f);
    
    Obj ob("assets/objs/uvmappedcow.obj", "assets/shaders/ObjFile.vert", "assets/shaders/ObjFile.frag");
    ob.createBuffers();
    ob.setObjColor(glm::vec3(1.0f, 0.5f, 0.31f));
    ob.setLightPosition(lightPosition);
    
    Obj cube("assets/objs/cube.obj", "assets/shaders/ObjFile.vert", "assets/shaders/White.frag");
    cube.createBuffers();
    cube.isLamp = true;
    
   
    
    while(!glfwWindowShouldClose(window)) {
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        
        processInput(window);

        
        glClear(GL_COLOR_BUFFER_BIT);

        glm::mat4 trans;
        
        glm::mat4 model;
        glm::mat4 perspective;
        glm::mat4 view;
        
        view = glm::lookAt(cameraPos, cameraPos + cameraFront, cameraUp);
        perspective = glm::perspective(glm::radians(50.0f), (float) (g_width / g_height), 0.1f, 100.0f);
        model = context.getModel();
        ob.draw(perspective, model, view, cameraPos);

        

        //faz cubo de luz
        model = glm::mat4();
        model = glm::translate(model, lightPosition);
        model = glm::scale(model, glm::vec3(0.2f));
        
        cube.draw(perspective, model, view);

        
        glfwSwapBuffers(window);
        glfwPollEvents();
    }
    glfwTerminate();
    
    return 0;
}

