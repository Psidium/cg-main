#ifndef MODEL_ROTATION_H
#define MODEL_ROTATION_H

//#include <glad/glad.h> // include glad to get all the required OpenGL headers
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
//#include <Shader.h>
//
//#include <string>
//#include <fstream>
//#include <sstream>
//#include <iostream>
//#include <vector>

#include "ModelState.h"

class ModelRotation : public ModelState {
public:
    void addY(glm::mat4* model, float speed);
    void subY(glm::mat4* model, float speed);
    void addX(glm::mat4* model, float speed);
    void subX(glm::mat4* model, float speed);
    void addZ(glm::mat4* model, float speed);
    void subZ(glm::mat4* model, float speed);
    ModelRotation();
};

#endif
