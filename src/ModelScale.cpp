#include "ModelScale.h"

void ModelScale::addX(glm::mat4 * value, float speed) {
    this->scaleUp(value, speed);
}

void ModelScale::addY(glm::mat4 * value, float speed) {
    this->scaleUp(value, speed);
}
void ModelScale::addZ(glm::mat4 * value, float speed) {
    this->scaleUp(value, speed);
}

void ModelScale::subX(glm::mat4 * value, float speed) {
    this->scaleDown(value, speed);
}

void ModelScale::subY(glm::mat4 * value, float speed) {
    this->scaleDown(value, speed);
}

void ModelScale::subZ(glm::mat4 * value, float speed) {
    this->scaleDown(value, speed);
}

void ModelScale::scaleUp(glm::mat4* value, float fVal) {
    *value = glm::scale(*value, glm::vec3(1/(fVal * 10)));
}

void ModelScale::scaleDown(glm::mat4* value, float fVal) {
    *value = glm::scale(*value, glm::vec3(fVal * 10));
}
