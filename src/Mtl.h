#ifndef MTL_H
#define MTL_H

#include <glad/glad.h> // include glad to get all the required OpenGL headers
#include <glm/glm.hpp>

#include <boost/filesystem.hpp>
#include <map>

class Mtl {
public:
    glm::vec3 ambientLightColor;
    glm::vec3 difuseLightColor;
    glm::vec3 specularLightColor;
    int Ns;
    int d;
    void loadTexture(boost::filesystem::path path);
    int getTextureIndex();
private:
    int textId;
};

class MtlLib {
public:
    MtlLib(boost::filesystem::path path);
    Mtl getMtlByName(std::string name);
private:
    std::map<std::string, Mtl> mtls;
};
  
#endif
