#ifndef MODEL_CONTEXT_H
#define MODEL_CONTEXT_H

#include <glad/glad.h> // include glad to get all the required OpenGL headers
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "ModelTranslate.h"
#include "ModelRotation.h"
#include "ModelState.h"
#include "ModelScale.h"

class ModelContext {
public:
    ModelContext();
    ~ModelContext();
    
    void setTranslationState();
    void setRotationState();
    void setScaleState();
    
    void addX(float speed);
    void addY(float speed);
    void addZ(float speed);
    void subX(float speed);
    void subY(float speed);
    void subZ(float speed);
    
    void scaleUp(float speed);
    void scaleDown(float speed);
    
    glm::mat4 getModel();
    
private:
    glm::mat4 model = glm::mat4(1);
    ModelState* state;
};

#endif
