#include "ModelContext.h"

ModelContext::ModelContext() {
    this->state = new ModelRotation();
}

ModelContext::~ModelContext() {
    delete this->state;
}

glm::mat4 ModelContext::getModel() {
    return glm::mat4(this->model);
}

void ModelContext::setScaleState() {
    delete this->state;
    this->state = new ModelScale();
}

void ModelContext::setRotationState() {
    delete this->state;
    this->state = new ModelRotation();
}

void ModelContext::setTranslationState() {
    delete this->state;
    this->state = new ModelTranslate();
}

void ModelContext::addX(float speed) {
    this->state->addX(&model, speed);
}

void ModelContext::addY(float speed) {
    this->state->addY(&model, speed);
}
void ModelContext::addZ(float speed) {
    this->state->addZ(&model, speed);
}

void ModelContext::subX(float speed) {
    this->state->subX(&model, speed);
}

void ModelContext::subY(float speed) {
    this->state->subY(&model, speed);
}

void ModelContext::subZ(float speed) {
    this->state->subZ(&model, speed);
}

void ModelContext::scaleUp(float speed) {
    ModelScale scale{};
    scale.scaleUp(&model, speed);
}

void ModelContext::scaleDown(float speed) {
    ModelScale scale{};
    scale.scaleDown(&model, speed);
}
