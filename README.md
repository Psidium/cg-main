To run:

```bash
mkdir build
cd build
cmake ..
# check for cmake errors, you must have GLFW3, GLEW and Boost installed system-wide for it to work
make
./mainOut
```
